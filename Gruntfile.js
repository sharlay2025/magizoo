'use strict';

module.exports = function (grunt) {

  grunt.initConfig({
    watch: {
      configFiles: {
        files: 'Gruntfile.js',
        options: {
          reload: true
        }
      },
      css: {
        files: ['scss/**/*.scss'],
        tasks: ['sass', 'postcss:dist', 'cssmin']
      },
      twig: {
        files: [ 'twig/*.twig', '**/*.twig', 'twig/data.json' ],
        tasks: ['twigRender'/*, 'prettify'*/]
      }
    },

    // "sass"-task configuration
    sass: {
      options: {
        sourceMap: true,
        outputStyle: 'expanded',
      },
      dist: {
        files: {
          'assets/css/app.css' : 'scss/app.scss'
        }
      }
    },
    autoprefixer: {
      options: {
        browsers: ['last 2 versions','ie >= 10']
      },
      dist: { // Target
        files: {
          'assets/css/app.css': 'assets/css/app.css'
        }
      }
    },

    postcss: {
      options: {
        map: true,
        processors: [
          require('autoprefixer'),
          require('postcss-flexbugs-fixes')
        ]
      },
      dist: {
        src: 'assets/css/app.css'
      }
    },

    cssmin: {
      target: {
        files: [{
          expand: true,
          cwd: 'assets/css',
          src: ['*.css', '!*.min.css'],
          dest: 'assets/css',
          ext: '.min.css'
        }]
      }
    },

    browserSync: {
      dev: {
        bsFiles: {
          src : [
            'assets/css/*.css',
            'assets/js/*.js',
            './*.html'
          ]
        },
        options: {
          watchTask: true,
          server: {
            baseDir: ["./"]
          }
        }
      }
    },

    twigRender: {
      dev: {
        files : [
          {
            data: './twig/data.json',
            expand: true,
            cwd: './twig/',
            src: ['**/*.twig', '!**/_*.twig'], // Match twig templates but not partials
            dest: './',
            ext: '.html'   // index.twig + datafile.json => index.html
          }
        ]
      }
    },

    prettify: {
      all: {
        expand: true,
        cwd: './',
        ext: '.html',
        src: ['*.html'],
        dest: './'
      }
    },

    svgmin: {
      options: {
        plugins: [
          {
            removeViewBox: false
          },
          {
            removeUselessStrokeAndFill: false
          }
        ]
      },
      multiple: {
        files: [{
          expand: true,
          cwd: 'assets/svg/',
          src: ['*.svg'],
          dest: 'assets/svg/'
        }]
      }
    }
  });

  // load all grunt tasks
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-twig-render');
  grunt.loadNpmTasks('grunt-prettify');
  grunt.loadNpmTasks('grunt-svgmin');

  // the default task (running "grunt" in console) is "default"
  grunt.registerTask('default', ['sass', 'postcss:dist', 'cssmin', 'twigRender', /*'prettify',*/ 'browserSync', 'watch']);
  grunt.registerTask('twig', ['twigRender']);
  grunt.registerTask('svg', ['svgmin']);
};