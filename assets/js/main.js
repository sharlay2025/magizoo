jQuery(document).ready(function($){
  /* Has touch */
  function hasTouch() {
    return 'ontouchstart' in document.documentElement
           || navigator.maxTouchPoints > 0
           || navigator.msMaxTouchPoints > 0;
  }

  /* Remove hash */
  function removeHash () { 
    var scrollV, scrollH, loc = window.location;
    if ("pushState" in history) {
      history.pushState("", document.title, loc.pathname + loc.search);
    }
    else {
      // Prevent scrolling by storing the page's current scroll offset
      scrollV = document.body.scrollTop;
      scrollH = document.body.scrollLeft;

      loc.hash = "";

      // Restore the scroll offset, should be flicker free
      document.body.scrollTop = scrollV;
      document.body.scrollLeft = scrollH;
    }
  }

  /* Header Menu on mobile */
  $(function() {
    var $headerMenuTrigger = $('[data-element="header-menu-trigger"]');
    var $headerMenuContent = $('[data-element="header-menu-content"]');
    var $body = $('html');

    $headerMenuTrigger.on('click', function(e){
      e.preventDefault();
      if ($body.hasClass('menu-active')) {
        $headerMenuContent.slideUp();
        $body.removeClass('menu-active');
      } else {
        $headerMenuContent.slideDown();
        $body.addClass('menu-active');
      }
    });

    $(document).keyup(function(e) {
      if (e.key === "Escape" || e.keyCode === 27) {
        $headerMenuContent.slideUp();
        $body.removeClass('menu-active');
      }
    });
  });

  /* Header search on mobile */
  $(function() {
    var $headerSearchTrigger = $('[data-element="header-search-trigger"]');
    var $headerSearchForm = $('[data-component="header-search-form"]');
    var $headerSearchClose = $('[data-element="header-search-form-close"]');

    $headerSearchTrigger.on('click', function(e){
      e.preventDefault();
      var $this = $(this);

      if ($this.hasClass('active')) {
        $headerSearchForm.slideUp();
        $this.removeClass('active');
      } else {
        $headerSearchForm.slideDown();
        $this.addClass('active');
      }
    });

    $headerSearchClose.on('click', function(e){
      e.preventDefault();
      var $this = $(this);

      $headerSearchForm.slideUp();
      $headerSearchTrigger.removeClass('active');
    });
  });

  /* Footer mobile nav */
  $(function() {
    function footerMobileNav() {
      var $footerNavToggle = $('[data-element="footer-nav-toogle"]');
      var activeClass = 'active';

      $footerNavToggle.on('click', function(e){
        e.preventDefault();

        var $this = $(this);

        var $component = $this.closest('[data-element="footer-nav"]');
        var $item = $this.closest('[data-element="footer-nav-item"]');
        var $content = $item.find('[data-element="footer-nav-list"]');
        var $siblings = $item.parent().siblings().find('[data-element="footer-nav-item"]');

        if ($item.hasClass(activeClass)) {
            $item.removeClass(activeClass);
          $content.slideUp();
        } else {
          $item.parent().siblings().find('[data-element="footer-nav-item"]').removeClass(activeClass);
          $item.parent().siblings().find('[data-element="footer-nav-list"]').slideUp();
          $item.addClass(activeClass);
          $content.slideDown();
        }
      });
    }

    footerMobileNav();
  });

  /* Catalog nav */
  $(function() {
    var $catalogToggle = $('[data-element="catalog-nav-toggle"]');
    var $body = $('html');
    var activeClass = 'active';

    if ($(window).width() < 1024) {
      $catalogToggle.on('click', function(e){
        e.preventDefault();

        let toggle = $(this);
        let item = toggle.closest('[data-component="catalog-nav"]');
        let subnav = item.find('[data-element="catalog-nav-content"]');

        item.toggleClass(activeClass);
        subnav.fadeToggle();
        if (subnav.find('[data-component="alpha-slider"]')) {
          $('[data-component="alpha-slider"]').slick('setPosition');
          $('[data-component="alpha-index-slider"]').slick('setPosition');
        }
        item.siblings().toggle();
      });
    } else if ($(window).width() >= 1024) {
      const $navItem = $('[data-component="catalog-nav"]');

      $navItem.hover(function() {
        let item = $(this);
        let activeSibling = item.siblings('.active');

        activeSibling.find('[data-element="nav-toggle"].active').removeClass('active');
        activeSibling.removeClass('active');

        if (item.find('[data-element="catalog-nav-content"]')) {
            $body.addClass('nav-active');
        }

        if (item.find('[data-component="alpha-slider"]')) {
          $('[data-component="alpha-slider"]').slick('setPosition');
          $('[data-component="alpha-index-slider"]').slick('setPosition');
        }
      },function() {
          let item = $(this);
          if (item.find('[data-element="catalog-nav-content"]')) {
              $body.removeClass('nav-active');
          }
      });
    }
  });

  /* Trigger mobile navigation */
  $(function() {
    var 
    $nav = $('[data-component="nav"]'),
    $navTrigger = $('[data-element="nav-trigger"]'),
    $navList = $('[data-element="nav-list"]'),
    activeClass = 'active';

    $navTrigger.on('click', function(e){
      e.preventDefault();
      $nav.toggleClass(activeClass);
      $navList.fadeToggle();
    });

    $(document).on('click', function (e) {
      if (!e.target.closest('[data-component="nav"]')) {
        $nav.removeClass(activeClass);
        $navList.hide();
      }
    });
  });

  /* Fixing header on scroll on desktop */
  $(function () {
    const $document = $(document);
    const $nav = $('[data-component="nav"]');
    const $navCol = $('[data-element="nav-col"]');
    const $header = $('[data-component="header"]');
    const $headerFixed = $('[data-component="header-fixed"]');
    const $body = $('html');
    const $firstMenuItem = $nav.find('[data-component="catalog-nav"]:first-child');

    const onScroll = debounce(function () {
      if ($(window).width() >= 1256) {
        if ($document.scrollTop() > 215) {
          $headerFixed.addClass('fixed');
          $body.addClass('header-fixed');
          $navCol.append($nav);
        } else {
          $headerFixed.removeClass('fixed');
          $body.removeClass('header-fixed nav-fixed nav-active');
          $nav.insertBefore($headerFixed);
          $firstMenuItem.removeClass('active');
          $firstMenuItem.find('[data-element="nav-toggle"]').removeClass('active');
        }

        $navCol.hover(function(e) {
          $firstMenuItem.addClass('active');
          $firstMenuItem.find('[data-element="nav-toggle"]').addClass('active');
        });
      }
    }, 10);

    $document.on('scroll', function () {
      onScroll();
    }).trigger('scroll');
  });

  /* Nav in fixed header on desktop */
  $(function () {
    const $body = $('html');
    const $navCol = $('[data-element="nav-col"]');

    $navCol.hover(function() {
      $body.toggleClass('nav-fixed');
    });
  });

  /* Menu */
  $(function() {
    if (hasTouch()) {

      $('[data-element="catalog-menu-card"]').on('click', function(e) {
        if (!e.target.href) {
          var $this = $(this);
          $this.parent().siblings().find('[data-element="catalog-menu-card"]').removeClass('active');
          $this.toggleClass('active');
        }
      });
    } else {
      $('[data-element="catalog-menu-card"]').hover(function(e) {
        $(this).toggleClass('active');
      });
    }
  });

  /* Favourite Item */
  $(function() {
    $('[data-element="item-fav"]').on('click', function(e) {
      e.preventDefault();
      var $this = $(this);
      if ($this.attr('data-state') === 'fav') {
        $this.removeAttr('data-state');
      } else {
        $this.attr('data-state', 'fav');
      }
    });
  });

  /* View all catalog item offers */
  $(function() {
    $('[data-element="catalog-more-offers-toggle"]').on('click', function(e) {
      var catalogItemPopup = $(this).closest('[data-element="item"]').find('[data-element="item-popup"]');
      catalogItemPopup.addClass('active');
    });
    $('[data-element="item-popup-close"]').on('click', function(e) {
      var catalogItemPopup = $(this).closest('[data-element="item-popup"]');
      catalogItemPopup.removeClass('active');
    });
    $(document).keyup(function(e) {
      if (e.key === "Escape" || e.keyCode === 27) {
        $('[data-element="item-popup"]').removeClass('active');
      }
    });
  });

  /* Filter */
  $(function() {
    var 
    $filters = $('[data-component="filters"]'),
    $filtersTrigger = $('[data-element="catalog-filters-trigger"]'),
    $filtersClose = $('[data-element="filters-close"]'),
    $filterToggle = $('[data-element="filter-toggle"]'),
    activeClass = 'active';

    $filtersTrigger.on('click', function(e){
      e.preventDefault();
      $filters.fadeIn();
    });

    $filtersClose.on('click', function(e){
      e.preventDefault();
      $filters.fadeOut();
    });

    $filterToggle.on('click', function(e){
      e.preventDefault();

      var $this = $(this);
      var $filter = $this.closest('[data-component="filter"]');
      var $filterContent = $filter.find('[data-element="filter-content"]');

      if ($filter.hasClass(activeClass)) {
        $filterContent.slideUp();
        $filter.removeClass(activeClass);
      } else {
        $filter.addClass(activeClass);
        $filterContent.slideDown();
      }
    });
  });

  /* Panel */
  $(function() {
    var $panelToggle = $('[data-element="panel-toggle"]');
    var $showMap = $('[data-element="panel-map"]');
    var activeClass = 'active';
    var $panelBtn = $('[data-element="map-switch"]');

    $panelToggle.on('click', function(e){
      e.preventDefault();

      var $this = $(this);
      var $panel = $this.closest('[data-component="panel"]');
      var $panelContent = $panel.find('[data-element="panel-content"]');

      $panel.toggleClass(activeClass);
      $panelContent.slideToggle();
    });

    $showMap.on('click', function(e){
      e.preventDefault();

      var $this = $(this);
      var $panel = $this.closest('[data-component="panel"]');
      var $panelContent = $panel.find('[data-element="panel-content"]');
      var $panelMap = $panel.find('.shop__map');
      var $panelImg = $panel.find('.shop__img');

      $panel.addClass(activeClass);
      $panelContent.slideDown();
      $panelBtn.text('Показать фото');
      $panelImg.hide();
      $panelMap.show();
    });

    $panelBtn.on('click', function(e){
      e.preventDefault();

      var $this = $(this);
      var $panel = $this.closest('[data-component="panel"]');
      var $panelContent = $panel.find('[data-element="panel-content"]');
      var $panelMap = $panel.find('.shop__map');
      var $panelImg = $panel.find('.shop__img');

      if ($this.text() === "Показать на карте") {
        $this.text('Показать фото');
      } else {
        $this.text('Показать на карте');
      }

      $panelImg.toggle();
      $panelMap.toggle();
    });

 
  });

  /* Accordion */
  $(function() {
    var $accordionToggle = $('[data-element="accordion-toggle"]');
    var activeClass = 'active';

    $accordionToggle.on('click', function(e) {
      e.preventDefault();

      var $this = $(this);
      var $item = $this.closest('[data-element="accordion-item"]');
      var $itemContent = $item.find('[data-element="accordion-content"]');

      if ($item.hasClass(activeClass)) {
        $itemContent.slideUp();
        $item.removeClass(activeClass);
      } else {
        $item.siblings().removeClass(activeClass);
        $item.siblings().find('[data-element="accordion-content"]').slideUp();
        $itemContent.slideDown();
        $item.addClass(activeClass);
      }
    });
  });

  /* Sliders */
  //$(function() {
    const $bannserSlider = $('[data-component="banner-slider"]');
    $bannserSlider.slick({
      dots: true,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            arrows: false,
          }
        },
      ]
    });

    const $catalogSlider = $('[data-component="catalog-slider"]');
    $catalogSlider.slick({
      infinite: false,
      dots: true,
      slidesToShow: 4,
      slidesToScroll: 4,
      responsive: [
        {
          breakpoint: 1256,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
          }
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            arrows: false,
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
          }
        },
      ]
    });

    const $catalogSliderMd = $('[data-component="catalog-slider-md"]');
    $catalogSliderMd.slick({
      infinite: false,
      slidesToShow: 3,
      slidesToScroll: 3,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            arrows: false,
            dots: true,
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
          }
        },
      ]
    });

    const $offersSlider = $('[data-component="offers-slider"]');
    $offersSlider.slick({
      infinite: false,
      dots: true,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            arrows: false,
          }
        },
      ]
    });

    const $offersSliderLg = $('[data-component="offers-slider-lg"]');
    $offersSliderLg.slick({
      infinite: false,
      dots: true,
      slidesToShow: 2,
      slidesToScroll: 2,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        },
      ]
    });

    const $entriesSlider = $('[data-component="entries-slider"]');
    $entriesSlider.slick({
      infinite: false,
      slidesToShow: 4,
      slidesToScroll: 4,
      dots: true,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            arrows: false,
            dots: true,
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            arrows: false,
            dots: true,
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
          }
        },
      ]
    });

    const $usefulSlider = $('[data-component="useful-slider"]');
    $usefulSlider.slick({
      infinite: false,
      slidesToShow: 4,
      slidesToScroll: 4,
      responsive: [
        {
          breakpoint: 1256,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            arrows: false,
            dots: true,
          }
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            arrows: false,
            dots: true,
          }
        },
        {
          breakpoint: 576,
          settings: 'unslick',
        },
      ]
    });

    const $boardSlider = $('[data-component="board-slider"]');
    $boardSlider.slick({
      infinite: false,
      dots: true,
      slidesToShow: 5,
      slidesToScroll: 5,
      responsive: [
        {
          breakpoint: 1256,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4,
            arrows: false,
          }
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            arrows: false,
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            arrows: false,
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
          }
        },
      ]
    });

    const $certificatesSlider = $('[data-component="certificates-slider"]');
    $certificatesSlider.slick({
      infinite: false,
      dots: true,
      slidesToShow: 6,
      slidesToScroll: 6,
      responsive: [
        {
          breakpoint: 1256,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 5,
            arrows: false,
          }
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4,
            arrows: false,
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            arrows: false,
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            arrows: false,
          }
        },
      ]
    });

    const $imageSlider = $('[data-component="image-slider"]');
    $imageSlider.slick({
      infinite: false,
      dots: true,
    });

    const $productSlider = $('[data-component="product-slider"]');
    $productSlider.slick({
      infinite: false,
      arrows: false,
      fade: true,
      asNavFor: '[data-component="product-slider-nav"]',
      responsive: [
        {
          breakpoint: 768,
          settings: {
            dots: true,
          }
        }
      ]
    });

    const $productSliderNav = $('[data-component="product-slider-nav"]');
    $productSliderNav.slick({
      infinite: false,
      slidesToShow: 4,
      slidesToScroll: 1,
      focusOnSelect: true,
      asNavFor: '[data-component="product-slider"]',
      responsive: [
        {
          breakpoint: 768,
          settings: "unslick"
        }
      ]
    });

    let slideTimer;
    $('[data-component="product-slider-nav"]').on('mouseenter', '.slick-slide', function (e) {
      var $currTarget = $(e.currentTarget);
      $('[data-component="product-slider-nav"] .slick-slide').removeClass('slick-current');
      $currTarget.addClass('slick-current');

      slideTimer = setTimeout(function () {
        var index = $('[data-component="product-slider-nav"]').find('.slick-current').data('slick-index');
        var slickObj = $('[data-component="product-slider"]').slick('getSlick');
        slickObj.slickGoTo(index);
      }, 500);
    }).on('mouseleave', '.slick-slide', function (e) {
      clearTimeout(slideTimer);
    }); 

    const $alphaSlider = $('[data-component="alpha-slider"]');
    $alphaSlider.slick({
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      mobileFirst: true,
      arrows: false,
      dots: true,
      responsive: [
        {
          breakpoint: 768,
          settings: "unslick"
        }
      ]
    });

    const $alphaIndexSlider = $('[data-component="alpha-index-slider"]');
    $alphaIndexSlider.slick({
      infinite: false,
      slidesToShow: 2,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
          }
        }
      ]
    });

    $(window).on('orientationchange resize', function() {
      $bannserSlider.slick('resize');
      $catalogSlider.slick('resize');
      $catalogSliderMd.slick('resize');
      $offersSlider.slick('resize');
      $offersSliderLg.slick('resize');
      $entriesSlider.slick('resize');
      $usefulSlider.slick('resize');
      $boardSlider.slick('resize');
      $certificatesSlider.slick('resize');
      $imageSlider.slick('resize');
      $productSlider.slick('resize');
      $productSliderNav.slick('resize');
      $alphaSlider.slick('resize');
      $alphaIndexSlider.slick('resize');
    });
  //});

  /* Masked Input */
  $(function() {
    $('[data-element="phone-mask"]').mask("+7 (999) 999-99-99");

    /* Masked input fix For parsley validation */
    $(document).on('keypress', function(evt) {
      if(evt.isDefaultPrevented()) {
        // Assume that's because of maskedInput
        // See https://github.com/guillaumepotier/Parsley.js/issues/1076
        $(evt.target).trigger('input');
      }
    });
  });

  /* Select */
  $(function() {
    $('[data-element="select"]').select2({
      minimumResultsForSearch: Infinity,
      containerCssClass: 'all',
      //dropdownCssClass: ':all:'
    });

    $('[data-element="select-sm"]').select2({
      minimumResultsForSearch: Infinity,
      containerCssClass: 'select2-sm',
    });

    $('[data-element="select-default"]').select2({
      minimumResultsForSearch: Infinity,
      containerCssClass: 'select2--dark',
    });

    /* Select2 For parsley validation */
    $('[data-element="select"], [data-element="select-sm"]').change(function() {
      $(this).trigger('input');
      $(this).parsley().validate();
    });

    $('[data-element="pickup-select"]').select2({
      minimumResultsForSearch: Infinity,
      templateResult: formatState,
      templateSelection: formatState
    });

    function formatState (opt) {
      if (!opt.id) {
        return opt.text.toUpperCase();
      } 

      var optimage = $(opt.element).attr('data-image'); 
      if(!optimage){
        return opt.text;
      } else {
        var $opt = $(
          '<span class="select-content"><img src="' + optimage + '" alt=""> ' + opt.text + '</span>'
        );
        return $opt;
      }
    };
  });

  /* Account form switch */
  $(function() {
    $('[data-element="account-form-switch"] input').change(function() {
        if (this.value == 'allot') {
            alert("Allot Thai Gayo Bhai");
        }
        else if (this.value == 'transfer') {
            alert("Transfer Thai Gayo");
        }
    });

    $(document).on('change', 'input[type="radio"][name="account-form-switch"]', function (e) {
      $('.account__form').toggleClass('active');
    });

  });

  /* Tabs */
  $(function() {
    var $tabs = $('[data-component="tabs"]'),
        $tabToggle = $('[data-component="tabs"] .tabs__link'),
        activeClass = 'active';

    $tabToggle.on('click', function(e) {
      e.preventDefault();

      var $this = $(this),
          tabID = $this.attr('href'),
          tabElement = '[data-id="' + tabID.slice(1) +'"]';

      if (!($this.hasClass(activeClass))) {
        $this.closest($tabs).find('.tabs__tab').removeClass(activeClass);
        $this.closest('.tabs__list').find('.' + activeClass).removeClass(activeClass);
        $this.closest($tabs).find(tabElement).addClass(activeClass);
        $this.addClass(activeClass);
        if ($this.closest($tabs).find(tabElement).find('[data-component="catalog-slider"]')) {
          $('[data-component="catalog-slider"]').slick('setPosition');
        }
      }
    });
  });

  /* Discount offer */
  $(function() {
    const $discountOffer = $('[data-component="discount-offer"]');
    const $discountOfferClose = $('[data-element="discount-offer-close"]');

    $discountOfferClose.on('click', function(e) {
      e.preventDefault();

      $discountOffer.hide();
    });
  });

  /* Scroll to */
  $.fn.scrollTo = function (speed, offset) {
      if (typeof(speed) === 'undefined')
          speed = 1000;

      if (typeof(offset) === 'undefined')
          offset = 0;

      $('html, body').animate({
          scrollTop: parseInt($(this).offset().top - offset)
      }, speed);
  };

  /* Collapsible Tabs */
  $(function() {
    var tabsSelector = '[data-component="collapsible-tabs"]',
    $tabsCollapsible = $(tabsSelector),
    $tabCollapsibleToggle = $tabsCollapsible.find('.tabs__link'),
    activeClass = 'active';

    $tabCollapsibleToggle.on('click', function(e) {
      e.preventDefault();

      var $tabToggle = $(e.target),
      tabCollapsibleActiveID = $tabToggle.attr('href'),
      tabCollapsibleActiveElement = '[data-id="' + tabCollapsibleActiveID.slice(1) +'"]',
      $tabsCollapsible = $tabToggle.closest(tabsSelector);

      if ($tabToggle.hasClass(activeClass)) {
        $tabsCollapsible.find(tabCollapsibleActiveElement).hide().removeClass(activeClass);
        $tabToggle.removeClass(activeClass);
      } else {
        $tabsCollapsible.find('.tabs__tab').hide().removeClass(activeClass);
        $tabsCollapsible.find('.tabs__link').removeClass(activeClass);
        $tabsCollapsible.find(tabCollapsibleActiveElement).addClass(activeClass).show();
        $tabToggle.addClass(activeClass);
        if ($(window).width() < 1024) {
          $tabToggle.scrollTo(500, 110);
        }
      }
    });

    tabsCollapsibleResize($tabsCollapsible, $tabCollapsibleToggle, activeClass);
    $(window).on('orientationchange resize', function() {
      tabsCollapsibleResize($tabsCollapsible, $tabCollapsibleToggle, activeClass);
    });

    function tabsCollapsibleResize($tabsCollapsible, $tabCollapsibleToggle, activeClass) {
      var $tabCollapsibleParent;

      if ($(window).width() < 1024) {
        $tabCollapsibleToggle.each(function (index, item) {
          var item = $(item),
          itemID = item.attr('href'),
          itemTab = item.closest($tabsCollapsible).find('[data-id="' + itemID.slice(1) +'"]');
          $tabCollapsibleParent = item.parent();
          itemTab.appendTo($tabCollapsibleParent);
        });
      } else {
        $tabCollapsibleToggle.each(function (index, item) {
          var item = $(item),
          itemID = item.attr('href'),
          itemTab = item.closest($tabsCollapsible).find('[data-id="' + itemID.slice(1) +'"]');
          $tabCollapsibleParent = item.closest($tabsCollapsible).find('.tabs__content');
          itemTab.appendTo($tabCollapsibleParent);
        });
      }
    }
  });

  /* Switch to tab */
  $(function() {
    $('[data-element="tab-toggle"]').on('click', function(e){
      e.preventDefault();
      var $this = $(this);
      var tabId = $this.attr('href');
      var tabsComponentId = $this.attr('data-component-id');
      var $tabsComponent = $('[data-component-id="'+tabsComponentId+'"]');
      $tabsComponent.find('.tabs__link.active').removeClass('active');
      $tabsComponent.find('.tabs__tab.active').removeClass('active');
      $tabsComponent.find('.tabs__link[href="'+tabId+'"]').addClass('active');
      $tabsComponent.find('.tabs__tab[data-id="' + tabId.slice(1) +'"]').addClass('active').show();
    });
  });

  /* Description toggle */
  $(function() {
    $('[data-element="description-toggle"]').on('click', function(e){
      e.preventDefault();
      var $this = $(this);
      var $description = $this.parent().siblings('[data-component="description"]');
      $description.removeClass('collapsed');
      $this.hide();
    });
  });

  /* Gallery Popup */
  $(function() {
    $('[data-gallery="true"]').each(function() {
      var $gallery = $(this);

      $gallery.magnificPopup({
        delegate: 'a',
        type: 'image', // this is a default type
        gallery: {
          enabled: true
        },
        closeOnContentClick: false,
        mainClass: 'mfp-zoom-in mfp-img-mobile',
        image: {
          verticalFit: true,
        },
        callbacks: {
          elementParse: function(item) {
            if(item.el[0].dataset.type == 'iframe') {
              item.type = 'iframe';
            } else {
              item.type = 'image';
            }
          },
        },
      });
    });
  });

  /* Modal with video */
  $(function() {
    $('[data-element="video-trigger"]').magnificPopup({
      type: 'iframe',
      mainClass: 'mfp-zoom-in',
      removalDelay: 160,
      preloader: false,
      gallery: {
        enabled: true
      },
      fixedContentPos: false
    });
  });

  /* Product Modal */
  $(function() {
    const $productSlider = $('[data-component="product-slider"]');
    const $productSliderNav = $('[data-component="product-slider-nav"]');
    const $productModal= $('[data-element="product-modal-trigger"]');

   $productModal.magnificPopup({
      type: 'inline',
      fixedContentPos: false,
      fixedBgPos: true,

      overflowY: 'auto',

      closeBtnInside: true,
      preloader: false,

      midClick: true,
      removalDelay: 0,
      mainClass: 'mfp-zoom-in',

      callbacks: {
        open: function() {
          $productSlider.slick('setPosition');
          $productSliderNav.slick('setPosition');

          let $productModalInstance = $.magnificPopup.instance;

          let $galleryItems = $.magnificPopup.instance.content.find('[data-gallery="true"] a');
          let itemsGallery = [];

          $galleryItems.each(function() {
            let el = $(this);
            let elType = 'image';
            let elTitle = el.attr('title');

            if (el.attr('data-type')) {
              elType = el.attr('data-type');
            }

            itemsGallery.push( {
              src: el.attr("href"),
              type: elType,
              title: elTitle,
            });
          });

          $galleryItems.on('click', function(ev){
            ev.preventDefault();

            $.magnificPopup.close();

            $.magnificPopup.open({
              items: itemsGallery,
              gallery: {
                enabled: true
              },
              type: 'image', // this is a default type
              closeOnContentClick: false,
              mainClass: 'mfp-zoom-in mfp-img-mobile',
              image: {
                verticalFit: true,
              },
              callbacks: {
                open: function() {
                  $.magnificPopup.instance.close = function() {

                    // Call the original close method to close the popup
                    $.magnificPopup.proto.close.call(this);

                    $productModal.magnificPopup('open');
                  };
                },
              },
            });
          });

          $productModalInstance.close = function() {
            // Call the original close method to close the popup
            $.magnificPopup.proto.close.call(this);
          };
        },
        close: function() {
          removeHash();
        }
      },
    });
  });

  /* Cart Modal */
  $(function() {
    const $body = $('html');
    const bodyCartClass = 'cart-modal-active';

    if (hasTouch()) {
      $('[data-element="cart-modal-trigger"]').magnificPopup({
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,

        overflowY: 'auto',

        preloader: false,

        midClick: true,

        callbacks: {
          open: function() {
            $body.addClass(bodyCartClass);
          },
          close: function() {
            $body.removeClass(bodyCartClass);
            removeHash();
          }
        }
      });
    } else {
      $('[data-element="cart-modal-trigger"]').on('mouseover', function(){
        $.magnificPopup.open({
          items: {
            src: $('#cart-modal'),
            type: 'inline',
          },
          fixedContentPos: false,
          fixedBgPos: true,

          overflowY: 'auto',

          midClick: true,

          callbacks: {
            open: function() {
              $body.addClass(bodyCartClass);
            },
            close: function() {
              $body.removeClass(bodyCartClass);
            }
          }
        });
      });
    }

    $(document).on('mouseover', function (e) {
      if (!$body.hasClass(bodyCartClass)) return false;
      if ($(e.target).closest('[data-element="cart-modal-trigger"]').length === 0 && $(e.target).closest('.mfp-wrap').length === 0) {
        $.magnificPopup.close();
      }
    });

    $('.cart-popup').on('mouseleave', function (e) {
      if (!$body.hasClass(bodyCartClass)) return false;
      $.magnificPopup.close();
    });

    /* Delete cart item */
    const $cartItemDeleteButton = $('[data-element="cart-popup-delete-item"]');

    $cartItemDeleteButton.on('click', function (e) {
      e.preventDefault();
      let $button = $(this);
      let $cartItem = $button.closest('[data-element="cart-popup-item"]');

      $cartItem.remove();
    });
  });

  /* Modal */
  $(function() {
    $(document).on('click', '[data-element="modal-trigger"]', function(e) {
      e.preventDefault();
      let $modalTrigger = $(this);
        $.magnificPopup.open({
          items: {
              src: $(this).attr('href'),
              type: 'inline',
          },
          fixedContentPos: false,
          fixedBgPos: true,

          overflowY: 'auto',

          closeBtnInside: true,
          preloader: false,

          midClick: true,
          removalDelay: 300,
          mainClass: 'mfp-zoom-in',
      });
    });

    $(document).on('click', '[data-element="modal-close"]', function (e) {
      e.preventDefault();
      $.magnificPopup.close();
    });
  });

  /* Location */
  $(function() {
    let $locationItem = $('[data-element="location-item"]');
    let $locationName = $('[data-element="location-name"]');

    $locationItem.on('click', function(e) {
      e.preventDefault();
      let $item = $(this);
      let itemName = $item.html();
      $locationName.html(itemName);
      $('.tippy-popper [data-element="location-name"]').html(itemName);
      $.magnificPopup.close();
    });
  });

  /* Button to top positioning */
  $(function() {
    var buttonToTop = $('[data-component="top-button"]');

    if (!buttonToTop) return false;

    var Toffset = 250;
    var Boffset = $('[data-component="footer"]').outerHeight();

    $(window).scroll(function(){
      if ($(this).scrollTop() > Toffset){
        buttonToTop.addClass("active");
      } else {
        buttonToTop.removeClass("active");
      }
    });
  });

  /* Amount form */
  $(function() {
    $(document).on('click', function(e) {
      var target = $(e.target);

      if (e.target.dataset.component == "amount-form") {
        var amountForm = target;
      } else if (target.parents('[data-component="amount-form"]').length) {
        var amountForm = target.parents('[data-component="amount-form"]');
      }

      if (!amountForm) return;

      var
        amountInput = amountForm.find('[data-element="amount-input"]'),
        amountValue = amountInput.val(),
        amountValueNumber = +amountValue,
        amountMin = +amountInput.attr('data-min')  || 0,
        amountMax = +amountInput.attr('data-max'),
        amountStep = +amountInput.attr('data-step') || 1,
        amountNewValue = 0;

        amountInput.focus().val('').val(amountValue);

      if (target.attr('data-element') === 'inc-control' || target.attr('data-element') === 'dec-control') {
        if (target.attr('data-element') === 'inc-control') {
          if (amountMax && amountValueNumber >= amountMax) {
            amountNewValue = amountValueNumber;
          } else {
            amountNewValue = amountValueNumber + amountStep;
          }
        } else if (target.attr('data-element') === 'dec-control') {
          if (amountValueNumber <= amountMin) {
            amountNewValue = amountValueNumber;
          } else {
            amountNewValue = amountValueNumber - amountStep;
          }
        }
        if (Number.isInteger(amountStep)) {
          amountInput.val(amountNewValue);
        } else {
          amountInput.val(amountNewValue.toFixed(1));
        }
        amountInput.blur();
      } else {
        amountInput.focus().val('').val(amountValue);
      }
    });
  });

  /* Product notes placement */
  $(function() {
    var $product = $('[data-component="product"]');
    var $productFooter = $('[data-element="product-footer"]');
    var $productMedia = $('[data-element="product-media"]');
    var $productNotes = $('[data-element="product-notes"]');

    productNotesPlacement();
    $(window).on('orientationchange resize', function() {
      productNotesPlacement();
    });

    function productNotesPlacement() {
      if ($(window).width() < 1024) {
        if ($productNotes.closest($productMedia).length == 1) {
          $productNotes.appendTo($productFooter);
        }
      } else {
        if ($productNotes.closest($productFooter).length == 1) {
          $productNotes.appendTo($productMedia);
          $productFooter.find('[data-element="product-notes"]').remove();
        }
      }
    }
  });

  /* Product info/sets toggle */
  $(function() {
    var $productToggle = $('[data-element="product-toggle"]');
    var $productInfo = $('[data-element="product-details"]');
    var $productSets = $('[data-element="product-sets"]');
    var productToggleInfoText = $productToggle.attr('data-info-text');
    var productToggleSetsText = $productToggle.attr('data-sets-text');

    $productToggle.on('click', function() {
      var $toggle = $(this);
      $toggle.text(function(i, v){
        return v === productToggleInfoText ? productToggleSetsText : productToggleInfoText;
      });
      $productSets.toggle();
      $productInfo.toggle();
    });
  });

  /* Form Parsley Validation */
  $(function() {
    $('[data-validate="parsley"]').parsley({
      excluded: "[disabled], :hidden",
      errorClass: 'has-error',
      successClass: 'has-success',
      errorsWrapper: '<div class="form__errors"></div>',
      errorTemplate: '<div class="form__error"></div>',
      errorsContainer (field) {
        return field.$element.closest('.form__group');
      },
      classHandler (field) {
        const $parent = field.$element.closest('.form__group');
        if ($parent.length) return $parent;

        return $parent;
      }
    });

    $('[data-validate="parsley-inline"]').parsley({
      errorClass: 'has-error',
      successClass: 'has-success',
      errorsWrapper: '<div class="form__errors"></div>',
      errorTemplate: '<div class="form__error"></div>',
      errorsContainer (field) {
        return field.$element.closest('.form__col');
      },
      classHandler (field) {
        const $parent = field.$element.closest('.form__group');
        if ($parent.length) return $parent;

        return $parent;
      }
    });
  });

  /* Form subscribe submit */
  $(function() {
    const $subscribeForm = $('[data-form="subscribe"]');

    $subscribeForm.on('submit', function(e) {
      e.preventDefault();
      const $form = $(this);
      if ( $form.parsley().isValid() ) {
        const data = $form.serialize();
        $form[0].reset();
        $('#subscribe-success-modal').magnificPopup({
          items: {
            src: $('#subscribe-success-modal')
          },
          type: 'inline',
          fixedContentPos: false,
          fixedBgPos: true,

          overflowY: 'auto',

          closeBtnInside: true,
          closeOnContentClick: true,
          preloader: false,

          midClick: true,
          removalDelay: 300,
          mainClass: 'mfp-zoom-in'
        }).magnificPopup('open');
        $('#subscribe-success-modal').on('click', function(){
          $(this).magnificPopup('close');
        });
      }
    });
  });

  /* Phone check */
  $(function() {
    const $form = $('[data-form]');
    const $formPhone = $form.find('[data-element="phone-mask"]');

    let timer = null;

    $formPhone.on('keydown', function(){
      clearTimeout(timer); 
      timer = setTimeout(validatePhone, 1000)
    });

    function validatePhone() {
      let phoneString = $formPhone.val();
      let number = phoneString.charAt(4);

      if (number === '7' || number === '8') {
        $formPhone.siblings('.form__message').text('Проверьте правильность ввода номера');
      } else {
        $formPhone.siblings('.form__message').text('');
      }
    }
  });

  /* Code auto focus */
  $(function() {
    function numberOnly(input) {
      let regex = /[^0-9]/gi;
      let newValue = input.val().replace(regex, "");
      input.val(newValue);
    }

    const $formCodeInputs = $('[data-component="form-code"] input');

    $formCodeInputs.on("input", function(){
      let $input = $(this);
      let $inputParent = $(this).parent();
      let $form = $inputParent.closest('form');
      numberOnly($input);
      setTimeout(function() {
        if ( $input.val().length >= parseInt($input.attr("maxlength"),10) )
          $inputParent.next().find('input').focus();

        if ($inputParent.next().length === 0) {
          $form.submit();
        }
      },0);

      $input.keyup(function(e) {
        if (e.key === 'Backspace' || e.key === 'Delete') {
          $inputParent.prev().find('input').focus();
        }
      });
    });
  });

  /* Form control section trigger */
  $(function() {
    const $sectionTrigger = $('[data-element="form-section-trigger"]');

    $sectionTrigger.on('change', function(e) {
      let trigger = $(this);
      let contentId = trigger.attr('data-id');
      let content = $('#'+contentId);
      let contentSiblings = content.siblings('[data-element="form-section-content"]');
      let component = trigger.closest('[data-component="form-section"]');

      if (trigger.is(':checked')) {
        if (trigger.is(':radio')) {
          contentSiblings.addClass('hidden');
        }
        content.removeClass('hidden');
        content.find('[data-parsley-group]').prop('disabled', false);
      } else {
        content.addClass('hidden');
        content.find('[data-parsley-group]').prop('disabled', true);
      }
    });
  });

  /* Review form toggle */
  $(function() {
    var $reviewFormToggle = $('[data-element="revew-form-toggle"]');

    $reviewFormToggle.on('click', function(e){
      e.preventDefault();

      var $button = $(this);
      var $emptyBlock = $button.closest('[data-component="empty-reviews"]');
      var $reviewForm = $emptyBlock.next('[data-form="review-form"]');

      $emptyBlock.hide();
      $reviewForm.slideDown();
    });
  });


  /* Letter in alpha list */
  $(function() {
    const $letters = $('[data-element="letter"]');
    const activeClass = 'active';
    const $letterAll = $('[data-letter="all"]');

    $letterAll.hide();

    $letters.on('click', function(e){
      e.preventDefault();

      const $letter = $(this);
      if ($letter.hasClass(activeClass)) {
        $(this).removeClass(activeClass);
        $letterAll.hide();
      } else {
        $letters.removeClass(activeClass);
        $letter.addClass(activeClass);
        $letterAll.show();
      }
    });
    $letterAll.on('click', function(e){
      e.preventDefault();
      $(this).hide();
    });
  });

  /* Brand Desc */

  $(function() {
    const $brandDesc = $('[data-component="brand-desc"]');
    const $brandDescToggle = $('[data-element="brand-desc-toggle"]');
    const textCollapsed = $brandDescToggle.attr('data-text-collapsed');
    const textOpen = $brandDescToggle.attr('data-text-open');
    const classCollapsed = 'collapsed';
    const classOpen = 'open';

    $brandDescToggle.on('click', function(e){
      e.preventDefault();

      if ($brandDesc.hasClass(classCollapsed)) {
        $brandDesc.removeClass(classCollapsed).addClass(classOpen);
        $brandDescToggle.html(textOpen);
      } else {
        $brandDesc.removeClass(classOpen).addClass(classCollapsed);
        $brandDescToggle.html(textCollapsed);
      }
    });
  });

  /* Brand history */
  $(function() {
    const $brandHistory = $('[data-component="brand-history"]');
    const $brandHistoryToggle = $('[data-element="brand-history-toggle"]');
    const textCollapsed = $brandHistoryToggle.attr('data-text-collapsed');
    const textOpen = $brandHistoryToggle.attr('data-text-open');
    const classCollapsed = 'collapsed';
    const classOpen = 'open';

    $brandHistoryToggle.on('click', function(e){
      e.preventDefault();

      if ($brandHistory.hasClass(classCollapsed)) {
        $brandHistory.removeClass(classCollapsed).addClass(classOpen);
        $brandHistoryToggle.html(textOpen);
      } else {
        $brandHistory.removeClass(classOpen).addClass(classCollapsed);
        $brandHistoryToggle.html(textCollapsed);
      }
    });
  });

  /* Brannds images switch */
  $(function() {
    var $brandsIndexControl = $('[data-element="brands-index-control"]');
    var brandsIndexClass = 'has-images';

    $brandsIndexControl.on('click', function(e) {
      e.preventDefault();

      var $control = $(this),
          controlTextOn = $control.attr('data-text-on'),
          controlTextOff = $control.attr('data-text-off'),
          $brandsIndex = $control.closest('[data-component="brands-index"]');
          $brandsIndexContent = $brandsIndex.find('[data-element="brands-index-content"]');

      if ($brandsIndex.hasClass(brandsIndexClass)) {
        $control.html(controlTextOn);
        $brandsIndex.removeClass(brandsIndexClass);
      } else {
        $control.html(controlTextOff);
        $brandsIndex.addClass(brandsIndexClass);
      }
    });
  });

  /* Show more component */
  $.fn.toggleText = function(t1, t2){
      if (this.text() == t1) {
          this.text(t2);
      } else {
          this.text(t1);
      }
      return this;
  };

  $(function() {
      let $showMoreToggle = $('[data-element="show-more-toggle"]');

      $showMoreToggle.on('click', function(e) {
          e.preventDefault();

          let toggle = $(this);
          let toggleTextOn = toggle.attr('data-text-on');
          let toggleTextOff = toggle.attr('data-text-off');
          let showMoreComponent = toggle.closest('[data-component="show-more-component"]');

          toggle.toggleText(toggleTextOn, toggleTextOff);

          showMoreComponent.find('[data-element="show-more-item"]').toggleClass('hidden');
      });
  });
});

/* Catalog slider item equal height headers */
$(window).on('load', function() {
  $(function() {
    const $catalogSlider = $('[data-component="catalog-slider"]');

    function catalogEqualHeightHeaders() {
      let winWidth = $(window).width();
      let maxHeight = 80;
  
      if (winWidth >= 768 && winWidth < 1024) {
        maxHeight = 72;
      } else if (winWidth >= 1024) {
        maxHeight = 74;
      }

      $catalogSlider.each(function() {
        let slider = $(this);
        let itemTitle = slider.find('[data-element="item-title"]');
        let itemHeader = slider.find('[data-element="item-header"]');

        if (winWidth >= 576) {
          let tallestTitle = Math.max.apply(Math, itemTitle.map(function(index, el) {
            $(el).css('height', '');
            return $(el).outerHeight();
          }));
  
          let tallestHeader = Math.max.apply(Math, itemHeader.map(function(index, el) {
            $(el).css('height', '');
            return $(el).outerHeight();
          }));

          if (tallestTitle > maxHeight) {
            tallestTitle = maxHeight;
          }
  
          itemTitle.each(function() {
            $(this).css('height', tallestTitle + 'px');
          });

          itemHeader.each(function() {
            $(this).css('height', tallestHeader + 'px');
          });
        } else {
          itemTitle.each(function() {
            $(this).css('height', '');
          });

          itemHeader.each(function() {
            $(this).css('height', '');
          });
        }
      });
    }

    catalogEqualHeightHeaders();

    $(window).on('orientationchange resize', function() {
      catalogEqualHeightHeaders();
    });
  });
});

/* Catalog list item equal heights */
(function(){
  let catalogList = document.querySelectorAll('[data-component="catalog-list"]');

  let sliceIntoChunks = function (array, chunkSize) {
    let chunks = [];
    let tempArray = null;

    for (let i = 0; i < array.length; i++) {
      if (i % chunkSize === 0) {
        tempArray = new Array();
        chunks.push(tempArray);
      }
      tempArray.push(array[i]);
    }

    return chunks;
  };

  let catalogListEqualHeights = function () {
    let winWidth = window.innerWidth;
    let maxHeight = 80;

    if (winWidth >= 768 && winWidth < 1024) {
      maxHeight = 72;
    } else if (winWidth >= 1024) {
      maxHeight = 74;
    }

    if (winWidth >= 576) {
      for (let i = 0; i < catalogList.length; i++){
        let catalogListItemsList = catalogList[i].querySelectorAll('[data-element="item"]');
        let catalogListTitlesList = catalogList[i].querySelectorAll('[data-element="item-title"]');
        let catalogListHeadersList = catalogList[i].querySelectorAll('[data-element="item-header"]');
        let catalogListSlice;
        let catalogListSliceMd = catalogList[i].getAttribute('data-slice-md');
        let catalogListSliceLg = catalogList[i].getAttribute('data-slice-lg');
        let catalogListSliceXl = catalogList[i].getAttribute('data-slice-xl');

        if (winWidth >= 576 && winWidth < 768) {
          catalogListSlice = 2;
        } else if (winWidth >= 768 && winWidth < 1024) {
          catalogListSlice = catalogListSliceMd;
        } else if (winWidth >= 1024 && winWidth < 1256) {
          catalogListSlice = catalogListSliceLg;
        } else if (winWidth >= 1256) {
          catalogListSlice = catalogListSliceXl;
        }

        if (catalogListTitlesList.length > 0) {
          let catalogListTitlesListSliced = sliceIntoChunks(catalogListTitlesList, catalogListSlice);

          for (let j = 0; j < catalogListTitlesListSliced.length; j++) {
            let catalogListTitles = [].slice.call(catalogListTitlesListSliced[j]);

            let tallestTitle = Math.max.apply(Math, catalogListTitles.map(function(title, index) {
              title.style.height = '';
              return title.offsetHeight;
            }));

            [].forEach.call(catalogListTitlesListSliced[j], (elem) => {
              if (tallestTitle > maxHeight) {
                tallestTitle = maxHeight;
              }
              elem.style.height = tallestTitle + 'px';
            });
          }
        }

        if (catalogListHeadersList.length > 0) {
          let catalogListHeadersListSliced = sliceIntoChunks(catalogListHeadersList, catalogListSlice);

          for (let j = 0; j < catalogListHeadersListSliced.length; j++) {
            let catalogListTitles = [].slice.call(catalogListHeadersListSliced[j]);

            let tallestHeader = Math.max.apply(Math, catalogListTitles.map(function(header, index) {
              header.style.height = '';
              return header.offsetHeight;
            }));

            [].forEach.call(catalogListHeadersListSliced[j], (elem) => {
              elem.style.height = tallestHeader + 'px';
            });
          }
        }

        if (catalogListItemsList.length > 0) {
          let catalogListItemsListSliced = sliceIntoChunks(catalogListItemsList, catalogListSlice);

          for (let j = 0; j < catalogListItemsListSliced.length; j++) {
            let catalogListItems = [].slice.call(catalogListItemsListSliced[j]);

            let tallestItem = Math.max.apply(Math, catalogListItems.map(function(item, index) {
              item.parentElement.style.height = '';
              return item.offsetHeight;
            }));

            [].forEach.call(catalogListItemsListSliced[j], (elem) => {
              elem.parentElement.style.height = tallestItem + 'px';
            });
          }
        }
      }
    } else {
      for (let i = 0; i < catalogList.length; i++){
        let catalogListItemsList = catalogList[i].querySelectorAll('[data-element="item"]');
        let catalogListTitlesList = catalogList[i].querySelectorAll('[data-element="item-title"]');
        let catalogListHeadersList = catalogList[i].querySelectorAll('[data-element="item-header"]');

        if (catalogListTitlesList.length > 0) {
          [].forEach.call(catalogListTitlesList, (elem) => {
            elem.style.height = '';
          });
        }
        if (catalogListHeadersList.length > 0) {
          [].forEach.call(catalogListHeadersList, (elem) => {
            elem.style.height = '';
          });
        }
        if (catalogListItemsList.length > 0) {
          [].forEach.call(catalogListItemsList, (elem) => {
            elem.parentElement.style.height = '';
          });
        }
      }
    }
  };

  if (catalogList.length > 0) {
    document.addEventListener('DOMContentLoaded', function() {
      window.addEventListener('load', catalogListEqualHeights, false);
      window.addEventListener('resize', catalogListEqualHeights, false);
    });
  }
})();

/* Countdown */
(function() {
  function getTimeRemaining(endtime) {
    var t = Date.parse(endtime) - Date.parse(new Date());
    var seconds = Math.floor((t / 1000) % 60);
    var minutes = Math.floor((t / 1000 / 60) % 60);
    var hours = Math.floor((t / (1000 * 60 * 60)));
    //var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
    var days = Math.floor(t / (1000 * 60 * 60 * 24));
    return {
      'total': t,
      'days': days,
      'hours': hours,
      'minutes': minutes,
      'seconds': seconds
    };
  }

  function initializeClock(el, endtime) {
    //var daysSpan = clock.querySelector('.timer__days');
    var hoursSpan = el.querySelector('.timer__hours');
    var minutesSpan = el.querySelector('.timer__minutes');
    var secondsSpan = el.querySelector('.timer__seconds');


    function updateClock() {
      var t = getTimeRemaining(endtime);

      //daysSpan.innerHTML = ('0' + t.days).slice(-2);
      hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
      minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
      secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

      if (t.total <= 0) {
        clearInterval(timeinterval);
      }
    }

    updateClock();
    var timeinterval = setInterval(updateClock, 1000);
  }

  var countdowns = document.querySelectorAll('[data-component="countdown"]');

  if (countdowns.length > 0) {
    countdowns = [...countdowns];

    for (var i = 0; i < countdowns.length; i++) {
      var deadline = countdowns[i].dataset.deadline;
      initializeClock(countdowns[i], deadline);
    }
  }
})();

/*! smooth-scroll v12.1.5 | (c) 2017 Chris Ferdinandi | MIT License | http://github.com/cferdinandi/smooth-scroll */
!(function(e,t){"function"==typeof define&&define.amd?define([],(function(){return t(e)})):"object"==typeof exports?module.exports=t(e):e.SmoothScroll=t(e)})("undefined"!=typeof global?global:"undefined"!=typeof window?window:this,(function(e){"use strict";var t="querySelector"in document&&"addEventListener"in e&&"requestAnimationFrame"in e&&"closest"in e.Element.prototype,n={ignore:"[data-scroll-ignore]",header:null,speed:500,offset:0,easing:"easeInOutCubic",customEasing:null,before:function(){},after:function(){}},o=function(){for(var e={},t=0,n=arguments.length;t<n;t++){var o=arguments[t];!(function(t){for(var n in t)t.hasOwnProperty(n)&&(e[n]=t[n])})(o)}return e},a=function(t){return parseInt(e.getComputedStyle(t).height,10)},r=function(e){"#"===e.charAt(0)&&(e=e.substr(1));for(var t,n=String(e),o=n.length,a=-1,r="",i=n.charCodeAt(0);++a<o;){if(0===(t=n.charCodeAt(a)))throw new InvalidCharacterError("Invalid character: the input contains U+0000.");t>=1&&t<=31||127==t||0===a&&t>=48&&t<=57||1===a&&t>=48&&t<=57&&45===i?r+="\\"+t.toString(16)+" ":r+=t>=128||45===t||95===t||t>=48&&t<=57||t>=65&&t<=90||t>=97&&t<=122?n.charAt(a):"\\"+n.charAt(a)}return"#"+r},i=function(e,t){var n;return"easeInQuad"===e.easing&&(n=t*t),"easeOutQuad"===e.easing&&(n=t*(2-t)),"easeInOutQuad"===e.easing&&(n=t<.5?2*t*t:(4-2*t)*t-1),"easeInCubic"===e.easing&&(n=t*t*t),"easeOutCubic"===e.easing&&(n=--t*t*t+1),"easeInOutCubic"===e.easing&&(n=t<.5?4*t*t*t:(t-1)*(2*t-2)*(2*t-2)+1),"easeInQuart"===e.easing&&(n=t*t*t*t),"easeOutQuart"===e.easing&&(n=1- --t*t*t*t),"easeInOutQuart"===e.easing&&(n=t<.5?8*t*t*t*t:1-8*--t*t*t*t),"easeInQuint"===e.easing&&(n=t*t*t*t*t),"easeOutQuint"===e.easing&&(n=1+--t*t*t*t*t),"easeInOutQuint"===e.easing&&(n=t<.5?16*t*t*t*t*t:1+16*--t*t*t*t*t),e.customEasing&&(n=e.customEasing(t)),n||t},u=function(){return Math.max(document.body.scrollHeight,document.documentElement.scrollHeight,document.body.offsetHeight,document.documentElement.offsetHeight,document.body.clientHeight,document.documentElement.clientHeight)},c=function(e,t,n){var o=0;if(e.offsetParent)do{o+=e.offsetTop,e=e.offsetParent}while(e);return o=Math.max(o-t-n,0)},s=function(e){return e?a(e)+e.offsetTop:0},l=function(t,n,o){o||(t.focus(),document.activeElement.id!==t.id&&(t.setAttribute("tabindex","-1"),t.focus(),t.style.outline="none"),e.scrollTo(0,n))},f=function(t){return!!("matchMedia"in e&&e.matchMedia("(prefers-reduced-motion)").matches)};return function(a,d){var m,h,g,p,v,b,y,S={};S.cancelScroll=function(){cancelAnimationFrame(y)},S.animateScroll=function(t,a,r){var f=o(m||n,r||{}),d="[object Number]"===Object.prototype.toString.call(t),h=d||!t.tagName?null:t;if(d||h){var g=e.pageYOffset;f.header&&!p&&(p=document.querySelector(f.header)),v||(v=s(p));var b,y,E,I=d?t:c(h,v,parseInt("function"==typeof f.offset?f.offset():f.offset,10)),O=I-g,A=u(),C=0,w=function(n,o){var r=e.pageYOffset;if(n==o||r==o||(g<o&&e.innerHeight+r)>=A)return S.cancelScroll(),l(t,o,d),f.after(t,a),b=null,!0},Q=function(t){b||(b=t),C+=t-b,y=C/parseInt(f.speed,10),y=y>1?1:y,E=g+O*i(f,y),e.scrollTo(0,Math.floor(E)),w(E,I)||(e.requestAnimationFrame(Q),b=t)};0===e.pageYOffset&&e.scrollTo(0,0),f.before(t,a),S.cancelScroll(),e.requestAnimationFrame(Q)}};var E=function(e){h&&(h.id=h.getAttribute("data-scroll-id"),S.animateScroll(h,g),h=null,g=null)},I=function(t){if(!f()&&0===t.button&&!t.metaKey&&!t.ctrlKey&&(g=t.target.closest(a))&&"a"===g.tagName.toLowerCase()&&!t.target.closest(m.ignore)&&g.hostname===e.location.hostname&&g.pathname===e.location.pathname&&/#/.test(g.href)){var n;try{n=r(decodeURIComponent(g.hash))}catch(e){n=r(g.hash)}if("#"===n){t.preventDefault(),h=document.body;var o=h.id?h.id:"smooth-scroll-top";return h.setAttribute("data-scroll-id",o),h.id="",void(e.location.hash.substring(1)===o?E():e.location.hash=o)}h=document.querySelector(n),h&&(h.setAttribute("data-scroll-id",h.id),h.id="",g.hash===e.location.hash&&(t.preventDefault(),E()))}},O=function(e){b||(b=setTimeout((function(){b=null,v=s(p)}),66))};return S.destroy=function(){m&&(document.removeEventListener("click",I,!1),e.removeEventListener("resize",O,!1),S.cancelScroll(),m=null,h=null,g=null,p=null,v=null,b=null,y=null)},S.init=function(a){t&&(S.destroy(),m=o(n,a||{}),p=m.header?document.querySelector(m.header):null,v=s(p),document.addEventListener("click",I,!1),e.addEventListener("hashchange",E,!1),p&&e.addEventListener("resize",O,!1))},S.init(d),S}}));

var scroll = new SmoothScroll('[data-scroll]', {
	speed: 1000,
	easing: 'easeInOutCubic',
  offset: 100,
});

/* Swipers */
(function() {
  const brandsSwiper = new Swiper('[data-component="brands-swiper"] .swiper-container', {
    slidesPerView: 6,
    slidesPerColumn: 3,
    slidesPerGroup: 6,
    spaceBetween: 24,
    navigation: {
      nextEl: '.swiper-next',
      prevEl: '.swiper-prev',
    },
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true,
    },
    breakpoints: {
      575: {
        slidesPerView: 2,
        slidesPerColumn: 2,
        slidesPerGroup: 2,
        spaceBetween: 16,
      },
      767: {
        slidesPerView: 3,
        slidesPerColumn: 3,
        slidesPerGroup: 3,
        spaceBetween: 16,
      },
      1023: {
        slidesPerView: 4,
        slidesPerColumn: 3,
        slidesPerGroup: 4,
        spaceBetween: 24,
      },
      1255: {
        slidesPerView: 5,
        slidesPerColumn: 3,
        slidesPerGroup: 5,
        spaceBetween: 24,
      },
    }
  });

  const brandsNavSwiper = new Swiper('[data-component="brands-nav-swiper"] .swiper-container', {
    slidesPerView: 6,
    slidesPerGroup: 6,
    spaceBetween: 24,
    observer: true,
    observeParents: true,
    navigation: {
      nextEl: '.swiper-next',
      prevEl: '.swiper-prev',
    },
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true,
    },
    breakpoints: {
      575: {
        slidesPerView: 2,
        slidesPerColumn: 3,
        slidesPerGroup: 3,
        spaceBetween: 16,
      },
      767: {
        slidesPerView: 3,
        slidesPerColumn: 3,
        slidesPerGroup: 3,
        spaceBetween: 16,
      },
      1023: {
        slidesPerView: 4,
        slidesPerColumn: 3,
        slidesPerGroup: 4,
        spaceBetween: 24,
      },
      1255: {
        slidesPerView: 5,
        slidesPerGroup: 5,
        spaceBetween: 24,
      },
    }
  });
})();

/* Perfect scrollbar */
(function() {
  const psContainers = document.querySelectorAll('[data-perfect-scrollbar]');

  if (!psContainers) {
    return false;
  }

  for (container of psContainers) {
    const ps = new PerfectScrollbar(container);
  }
})();

/* Popover */
tippy('[data-element="popover-trigger"]', {
  maxWidth: 324,
  theme: 'light',
  distance: 17,
  content(reference) {
    const id = reference.getAttribute('data-template');
    const template = document.getElementById(id);
    return template.innerHTML;
  },
  onShown(instance) {
    const $packingItems = document.querySelectorAll('[data-element="packing-item"]');
    if ($packingItems) {
      $packingItems.forEach(function(item) {
        item.addEventListener("click", function(e) {
          e.preventDefault();
          let clickedItem = this;
          let component = clickedItem.closest('[data-component="packing"]');
          let choiceHolder = component.querySelector('[data-element="packing-choice"]');
          if(e.target && e.target == clickedItem){
            let clickedValue = clickedItem.innerHTML;
            component.querySelector('.active').classList.remove('active');
            clickedItem.classList.add('active');
            choiceHolder.innerHTML = clickedValue;
            instance.hide();
          }
        });
      });
    }
  }
});

/* Tooltip */
tippy('[data-element="tooltip-trigger"]', {
  maxWidth: 324,
  theme: 'light',
  distance: 10
});

/* Open login popup for new user on page load */
$(window).on('load', function() {
  $(function() {
    const $loginPopup = $('#modal-login-new');

    if ($loginPopup.length > 0) {
      $.magnificPopup.open({
        items: {
          src: '#modal-login-new',
          type: 'inline',
        },
        fixedContentPos: false,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 100,
        mainClass: 'mfp-zoom-in',
      });
    }
  });
});

/* Cookies popup */
$(document).ready(function(){
  const $cookiesPopup = $('[data-component="cookies-popup"]');
  const $cookiesPopupClose = $('[data-element="cookies-popup-close"]');
  let cookiesPopupLS = localStorage.getItem('cookiesPopup');
  const hiddenClass = "is-hidden";
  const visibleClass = "is-visible";

  if (cookiesPopupLS != null) {
    if (cookiesPopupLS === hiddenClass) {
      $cookiesPopup.removeClass(visibleClass);
    } else {
      $cookiesPopup.addClass(visibleClass);
    }
  } else {
    $cookiesPopup.addClass(visibleClass);
  }

  $cookiesPopupClose.on('click', function(e) {
    e.preventDefault();

    if ($cookiesPopup.hasClass(visibleClass)) {
        $cookiesPopup.removeClass(visibleClass);
        localStorage.setItem('cookiesPopup', hiddenClass);
    }
  });
});

/* Product image zoom */
document.addEventListener('DOMContentLoaded', function() {
    let zoomImgs = document.querySelectorAll('[data-zoom-img]');

    if (zoomImgs.length > 0) {
        zoomImgs.forEach(function (image) {
            let imageParent = image.closest('[data-zoom-parent]');
            let imageProduct = image.closest('.product');
            let imagePaneContainer = imageProduct.querySelector('[data-zoom-pane]');

            new Drift(image, {
                namespace: 'product',
                paneContainer: imagePaneContainer,
                inlinePane: false,
                hoverBoundingBox: true,
                onShow: function() {
                    let imageParentHeight = imageParent.offsetHeight;

                    imagePaneContainer.style.height = imageParentHeight + 'px';
                },
                onHide: function() {
                    imagePaneContainer.style.height = '';
                },
            });
        });
    }
});
