jQuery(document).ready(function($){
  const $loginForm = $('[data-form="login"]');

  $loginForm.on('submit', function(e) {
    e.preventDefault();
    const $form = $(this);
    if ( $form.parsley().isValid() ) {
      const phone = $form.find('input[name="phone"]').val();
      const $phoneElement = $('[data-form="login-code"] [data-element="phone-number"]');

      $phoneElement.html(phone);

      $.magnificPopup.instance.close = function () {
        // "proto" variable holds MagnificPopup class prototype
        // The above change that we did to instance is not applied to the prototype, 
        // which allows us to call parent method:
        $.magnificPopup.proto.close.call(this);
      };

      $.magnificPopup.instance.open = function(data) {
          $.magnificPopup.proto.open.call(this,data);
          const $codeFirstInput = $('#first-number');
          if (!$codeFirstInput) return false;
          $codeFirstInput.focus();
      };

      $.magnificPopup.open({
        items: {
          src: $('#modal-login-code'),
          type: 'inline',
        },
        fixedContentPos: false,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in',
      });
    }
  });
});