ymaps.ready()
.done(function (ym) {
  var myMap = new ym.Map('map', {
    center: [55.751574, 37.573856],
    zoom: 10,
    controls: []
  });

  jQuery.getJSON('/assets/js/map-data.json', function (json) {
    var geoObjects = ym.geoQuery(json);
    geoObjects.searchInside(myMap)
    .addToMap(myMap);
    myMap.events.add('boundschange', function () {
        // После каждого сдвига карты будем смотреть, какие объекты попадают в видимую область.
        var visibleObjects = geoObjects.searchInside(myMap).addToMap(myMap);
        // Оставшиеся объекты будем удалять с карты.
        geoObjects.remove(visibleObjects).removeFromMap(myMap);
    });
  });
});