jQuery(document).ready(function($){
  /* Date picker */
  moment.locale('ru');
  var start = moment().add(1, 'days');
  var datePicker = $('[data-element="single-datepicker-input"]').daterangepicker({
      "singleDatePicker": true,
      "autoApply": true,
      "startDate": start,
      "endDate": start,
      "locale": {
            "format": "DD.MM.YY",
            "separator": " - ",
            "daysOfWeek": [
                "Вс",
                "Пн",
                "Вт",
                "Ср",
                "Чт",
                "Пт",
                "Сб"
            ],
            "monthNames": [
                "Январь",
                "Февраль",
                "Март",
                "Апрель",
                "Май",
                "Июнь",
                "Июль",
                "Aвгуст",
                "Сентябрь",
                "Октябрь",
                "Ноябрь",
                "Декабрь"
            ],
            "firstDay": 1
        }
  });

  const $orderForm = $('[data-form="order"]');
  const $deliveryInfo = $('[data-element="order-form-delivery-info"]');
  const $editButton = $('[data-element="order-form-edit"]');
  const $submit = $('[data-element="order-form-submit"]');
  const $firstSection = $('[data-component="form-panel"]:first-child');

  let scroll = new SmoothScroll('.some-selector',{
    header: '[data-component="nav"]',
    speed: 1000, easing: 'easeOutCubic', offset: 16
  });

  $('[data-element="form-panel-next"]').on('click', function(e){
    e.preventDefault();
    const $this = $(this);
    const panelGroupId = $this.attr('data-group');
    const $panel = $('#'+ panelGroupId);
    const $panelNext = $panel.next();
    const panelNextId = $panel.next().attr('id');

    $orderForm.parsley().validate({group:panelGroupId, force: false});

    if ($orderForm.parsley().isValid({group:panelGroupId, force: false})) {
      $panel.addClass('collapsed');
      $panelNext.removeClass('collapsed');

      scroll.animateScroll(document.querySelector('#'+panelNextId));

      if (panelGroupId === 'delivery') {
        $deliveryInfo.prop('hidden', false);
      }

      if (panelGroupId === 'payment') {
        $editButton.prop('hidden', false);
        $submit.prop('hidden', false);
      }
    }
  });

  $('[data-element="form-panel-prev"]').on('click', function(e){
    e.preventDefault();
    const $this = $(this);
    const panelGroupId = $this.attr('data-group');
    const $panel = $('#'+ panelGroupId);
    const $panelPrev = $panel.prev();
    const panelPrevId = $panel.prev().attr('id');

    $panel.addClass('collapsed');
    $panelPrev.removeClass('collapsed');

    scroll.animateScroll(document.querySelector('#'+panelPrevId));
  });

  $(document).on('click', '[data-element="order-form-edit"]', function (e) {
    e.preventDefault();
    $firstSection.removeClass('collapsed');
    $deliveryInfo.prop('hidden', true);
    $editButton.prop('hidden', true);
    $submit.prop('hidden', true);
    scroll.animateScroll(document.querySelector('#top'));
  });

  $orderForm.on('submit', function(e) {
    e.preventDefault();
    const $form = $(this);
    if ( $form.parsley().isValid() ) {
      //const data = $form.serialize();
      //$form[0].reset();
      $('#modal-notification').magnificPopup({
        items: {
          src: $('#modal-notification')
        },
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: true,
        closeOnContentClick: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in'
      }).magnificPopup('open');
      $('#modal-notification').on('click', function(){
        $(this).magnificPopup('close');
      });
    }
  });
});