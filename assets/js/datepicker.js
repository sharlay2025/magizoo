jQuery(document).ready(function($){
  /* Date picker */
  moment.locale('ru');
  var start = moment();
  var picker = $('[data-element="subscribe-datepicker-input"]').daterangepicker({
      "singleDatePicker": true,
      "alwaysShowCalendars": true,
      "parentEl": '[data-element="subscribe-datepicker-calendar"]',
      "autoApply": true,
      "startDate": start,
      "endDate": start,
      "locale": {
            "format": "DD MMMM YYYY",
            "separator": " - ",
            "daysOfWeek": [
                "Вс",
                "Пн",
                "Вт",
                "Ср",
                "Чт",
                "Пт",
                "Сб"
            ],
            "monthNames": [
                "Январь",
                "Февраль",
                "Март",
                "Апрель",
                "Май",
                "Июнь",
                "Июль",
                "Aвгуст",
                "Сентябрь",
                "Октябрь",
                "Ноябрь",
                "Декабрь"
            ],
            "firstDay": 1
        }
  });

  // range update listener
  picker.on('apply.daterangepicker', function(e, picker) {
    var chosenDay = +picker.startDate.format('D');
    var preposition = 'c ';
    if (chosenDay === 2) {
      preposition = 'cо ';
    }
    
    var chosenDate = picker.startDate.format('LL');
    chosenDate = chosenDate.substring(0, chosenDate.length - 8);
    chosenDate = preposition + chosenDate + picker.startDate.format(' (dddd)');
    $('[data-element="subscribe-datepicker-result"]').html(chosenDate);
  });
  // prevent hide after range selection
  picker.data('daterangepicker').hide = function () {};
  // show picker on load
  picker.data('daterangepicker').show();
});