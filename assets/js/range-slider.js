jQuery(document).ready(function($){
    let $rangeSlider = $('[data-element="range-slider-input"]');

    function formatValue(value) {
        return value.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }

    function toInt(value) {
        return +value.toString().replace(/ /g, '');
    }

    $rangeSlider.each(function(){
        let $range = $(this);
        let $rangeComponent = $range.closest('[data-component="range-slider"]');
        let $rangeInput = $rangeComponent.find('[data-element="range-slider-input"]');
        let $rangeFrom = $rangeComponent.find('[data-element="range-slider-from"]');
        let $rangeTo = $rangeComponent.find('[data-element="range-slider-to"]');
        let instance;

        $rangeFrom.val(formatValue($rangeFrom.val()));
        $rangeTo.val(formatValue($rangeTo.val()));

        $range.ionRangeSlider({
            type: "double",
            prettify_enabled: false,
            hide_min_max: true,
            hide_from_to: true,
            grid: false,
            onChange: function (data) {
                from = data.from;
                to = data.to;

                $rangeFrom.val(formatValue(from));
                $rangeTo.val(formatValue(to));
            }
        });

        instance = $range.data("ionRangeSlider");

        let min = instance.result.min;
        let max = instance.result.max;
        let from = instance.result.from;
        let to = instance.result.to;

        var updateRange = function () {
            instance.update({
                from: from,
                to: to
            });
        };

        $rangeFrom.on("change", function () {
            from = toInt($(this).val());

            if (from < min) {
                from = min;
            }

            if (from > to) {
                from = to;
            }

            updateRange();

            $rangeFrom.val(formatValue(from));
        });

        $rangeTo.on("change", function () {
            to = toInt($(this).val());

            if (to > max) {
                to = max;
            }

            if (to < from) {
                to = from;
            }

            updateRange();

            $rangeTo.val(formatValue(to));
        });
    });
});