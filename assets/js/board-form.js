Dropzone.autoDiscover = false;

jQuery(document).ready(function($){
  $('#board-dropzone').dropzone({
    url: '#',
    maxFiles:   10,
    uploadMultiple: false,
    autoProcessQueue:false,
    addRemoveLinks: true,
    dictRemoveFile: 'Удалить',
    dictMaxFilesExceeded: 'Вы можете загрузить не больше 10 файлов',
    init: function(){
      var myDropzone = this;
    }
  });
});